%makeOptRelationTable

bootStrapAmount=10
totalRow=size(myMatCostPer,1);
uopc_codes=size(myMatCostPer,2);
t1=10
Zout=zeros(uopc_codes,uopc_codes);
for i=1:uopc_codes
    tic
    ((uopc_codes-i)*t1)/3600
    for j=1:uopc_codes       %loop through all columns
       %First i, then j?
       pre=myMatCostPer(:,i)>0;
       post=myMatCostPer(:,j)>0;
       fracVec=zeros(1,bootStrapAmount);
       for bootstrap=1:bootStrapAmount
            pick=myMatEnd(:,i)<myMatStart(:,j)&pre==1&post==1&rand(totalRow,1)<0.8;   %randomly select
            fracVec(bootstrap)=sum(pick==1)/totalRow;
       end %end bootstrap
       if median(fracVec)>0
           Zout(i,j)=median(fracVec);   %most representative output
           %keyboard
       end
    end
    t1=toc;
end