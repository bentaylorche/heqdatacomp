

%include type
%include cost
%include time.... start?

umemberIDnew=unique(NewMemberIDnew);                       %unique
uCPTcode=unique(CPTCode);
%init matrix
myMat=zeros(length(umemberIDnew),length(uCPTcode));
myMatCost=myMat;
myMatEnd=myMat;
myMatStart=myMat;
myMatType=myMat;

colPick=zeros(length(umemberIDnew),1);                  %each column pick

t1=10;
for g=1:length(uCPTcode)                            %Loop through unique CPTcodes
    tic
    tleft=((length(uCPTcode)-g)*t1)/60;
    [g/length(uCPTcode) tleft]
%    g=round(length(uCPTcode)*rand)
    %tempStr=char(CPTCode(g));
    pick=ismember(CPTCode,uCPTcode(g));              %Where is this code used?
    %Now get claimIDs
    tempClaims=claimID_cpt(pick);                    %Find claims with this code...
    %Now get unique memberIDs+dependents (more observations)
    %claimIDcpt(pick)
    [C,ia,ib]=intersect(ClaimID,tempClaims);        %Find rows with these claims
    
    tempMembers=NewMemberIDnew(ia);                        %select rows out
    tempCosts=RepricedAmount(ia);    
    tempCostsEnd=ServiceEndNum(ia);
    tempCostsStart=ServiceStartNum(ia);
    tempType=ClaimTypeNum(ia);
    
    %Initi variables
    colPickTemp=colPick;
    colPickTempCost=colPick;
    colPickTempEnd=colPick;
    colPickTempStart=colPick;
    colPickTempType=colPick;
    
    %Now loop through each member?
    for member=1:length(tempMembers)
        member/length(tempMembers)
        findRow=find(umemberIDnew==tempMembers(member)); %get row...
        colPickTemp(findRow)=1;                                                         % true...
        colPickTempCost(findRow)=mean(tempCosts((tempMembers==tempMembers(member))));   % take mean cost
        colPickTempEnd(findRow)=min(tempCostsEnd((tempMembers==tempMembers(member)))); 
        colPickTempStart(findRow)=min(tempCostsStart((tempMembers==tempMembers(member)))); 
        colPickTempType(findRow)=mode(tempType((tempMembers==tempMembers(member)))); 
    end
    %keyboard

    myMat(:,g)=colPickTemp;
    myMatCost(:,g)=colPickTempCost;
    myMatEnd(:,g)=colPickTempEnd;
    myMatStart(:,g)=colPickTempStart;
    myMatType(:,g)=colPickTempType;
%    imagesc(myMat(:,1:g))
%    colormap gray
%    drawnow
    t1=toc;
end %end loop
keyboard
