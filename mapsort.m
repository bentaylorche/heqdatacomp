
% This function takes the matrix which is
% unique_member_id x unique_OCT_code

% and begins in the top left corner and then procedes to sort along the diagonal. The output produces
% clusters of frequent codes and similar codes for further research. The pixel value (color) is defined by the cost percentile

%Flow, get data, make cost percentile


%Warnick shuffle
mapsortRef
figure;
%f = figure('visible','off');
j=1;
%xind=1:size(C3,2);
%yind=1:size(C3,1);
t1=10;
for j=1:size(C3,1)
    [j/size(C3,1) ((size(C3,1)-j)*t1)/3600 ((900-j)*t1)/3600 ]
    tic
    j
    [sa si]=sort(C3(j,j:end),'descend');  %row [sort current column]
    C3(:,j:end)=C3(:,si+(j-1));
    C4(:,j:end)=C4(:,si+(j-1));
    xind(j:end)=xind(si+(j-1));
    j=j+1;
    [sa si]=sort(C3(j:end,j-1),'descend'); % ,'descend' [sort current row]
    C3(j:end,:)=C3(si+(j-1),:);
    C4(j:end,:)=C4(si+(j-1),:);
    yind(j:end)=yind(si+(j-1));

    %Convert cost value to percentile? 0 = 0.1-0.99 HOT
    if j==900
        keyboard
    end
    %if rand>0.9
        %subplot(1,2,1);
        %hold off;
        %imagesc(C3(1:j,1:j));
        %axis tight
        %colormap gray
        %subplot(1,2,2);
        hold off;
        try
        imagesc(C4(1:j,1:j));
        catch
            keyboard
        end
        colormap hot
        drawnow
        set(gca,'YTick',[],'XTick',[])
        set(gca,'position',[0 0 1 1])
        %saveas(f, ['image_cache/file_' num2str(j) '.png'],'png');
    %end
    t1=toc;
end
