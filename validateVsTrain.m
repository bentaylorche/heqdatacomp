
format compact
%Make histograms
[yhs,xhs]=hist(Train,10000);
yhs=[0 yhs 0];
xhs=[min(xhs) xhs max(xhs)];

plot(xhs,yhs/max(yhs),'-')
hold all;

low=quantile(Train,0.50)
med=quantile(Train,0.75)
medhigh=quantile(Train,0.95)
high=quantile(Train,0.999)
%veryhigh=quantile(Train,0.9999)

plot([low low],[0 1],'r--')
plot([med med],[0 1],'r--')
plot([medhigh medhigh],[0 1],'r--')
plot([high high],[0 1],'r--')

TrainFlag=Train*0;
TrainFlag(Train<low)=1;
TrainFlag(Train>=low&Train<med)=2;
TrainFlag(Train>=med&Train<medhigh)=3;
TrainFlag(Train>=medhigh&Train<high)=4;
TrainFlag(Train>=high)=5;

%Now bucket new data? How many are right?
ValidateFlag=Validate*0;
ValidateFlag(Validate<low)=1;
ValidateFlag(Validate>=low&Validate<med)=2;
ValidateFlag(Validate>=med&Validate<medhigh)=3;
ValidateFlag(Validate>=medhigh&Validate<high)=4;
ValidateFlag(Validate>=high)=5;

%How many buckets right?
compare=(ValidateFlag-TrainFlag);

%(compare>5)*5+
median(abs(Validate-Train))
mean(abs(Validate-Train))

%penalty matrix
penalty=[0 2 4 6 8;
1 0 2 4 6;
2 1 0 2 4;
3 2 1 0 2;
4 3 2 1 0];

totalPen=0;
for j=1:length(TrainFlag) %loop through all, check prediction
    totalPen=totalPen+penalty(TrainFlag(j),ValidateFlag(j));
end
totalPen/length(TrainFlag) 




