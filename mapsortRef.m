
%Warnick shuffle

figure;
%figure('units','normalized','outerposition',[0 0 1 1])
%set(gcf, 'color', [1 1 1])
j=1;
xind=1:size(C3,2);
yind=1:size(C3,1);

for procedure=1:size(C3,2)
    track(procedure)=sum(C3(:,procedure));
end
[sa si]=sort(track,'descend');  %row
C4=C4(:,si);
C3=C3(:,si);
xind=xind(si);

for patient=1:size(C3,1)
    track2(patient)=sum(C3(patient,:));
end
[sa si2]=sort(track2,'descend');  %row
C4=C4(si2,:);
C3=C3(si2,:);
yind=yind(si2);

%Now track patient sums
imagesc(C4);
axis tight
drawnow
% 
% for j=1:size(C3,1)
%     j
%     %Get max array
%     [sa si]=sort(C3(j:end,j-1),'descend'); % ,'descend'
%     C3(j:end,:)=C3(si+(j-1),:);
%     yind(j:end)=yind(si+(j-1));
%     %if rand>0.9
%     hold off;
%     imagesc(C3(1:j,1:j));
%     axis tight
%     drawnow
%     %end
% end