%Convert to numeric array
function [dataInNum,ustring]=cat2num(dataIn)

ustring={};                                 %unique string mapper
dataInNum=zeros(1,length(dataIn));
for j=1:length(dataIn)
    (j/length(dataIn))*100
    if sum(ismember(ustring,char(dataIn(j))))>0
        dataInNum(j)=find(ismember(ustring,char(dataIn(j))));
    else
        newIndex=length(ustring)+1;
        ustring{newIndex}=char(dataIn(j))  %add it, now check
        dataInNum(j)=find(ismember(ustring,char(dataIn(j))));
    end
%    keyboard
end