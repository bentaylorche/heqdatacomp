


%Get date?
step=max(DateProcessedNum(1:end-1))-DateProcessedNum(1);
transDate=DateProcessedNum(1)+step*0.7;   %train on 70%


firstGap=transDate-DateProcessedNum(1);
finalGap=max(DateProcessedNum(1:end-1))-transDate;

um=unique(mapMemID);
smallestDate=DateProcessedNum(1);
Train=zeros(1,length(um)-1);            %initialize memory
Validate=zeros(1,length(um)-1);         %initialize memory

myMatTrain=myMatCostPer*0;              %init VAL train
myMatVal=myMatCostPer*0;                %init VAL matrix

for j=1:length(um)-1 %loop through all unique members, except last
    j/length(um)
    hold all;
    pick=mapMemID==um(j);

    agep=ageCol(j);
    gendp=genderCol(j);
    
    x=DateProcessedNum(pick);
    y=RepricedAmount(pick);
    [sa si]=sort(x);
    x=sa;
    y=y(si);
    %plot(x(x>smallestDate),y(x>smallestDate),'.-')
    %hold all;
    %plot([transDate transDate],[min(y) max(y)],'--k','linewidth',3)
    %datetick('x','mm/YY')
    
    %Now get median of training data...
    tempData=y(x>smallestDate&x<=transDate);
    if length(tempData)>0
        Train(j)=sum(tempData)/firstGap;  %spend per day
    else
        Train(j)=0;  %no information
    end
    rowPick=myMatStart(j,:)<transDate;
    myMatTrain(j,rowPick)=myMatCostPer(j,rowPick); %populate what existed
    XTrain(j,1)=agep;
    XTrain(j,2)=gendp;
    XTrain(j,3)=max(myMatTrain(j,:));
    
    tempData2=y(x>transDate);  %since
    %if sum(isnan(tempData))>0
    %    keyboard
    %end
    
    %no matter what you have to walk through the data week by week....
    
    if length(tempData2)>0
        Validate(j)=sum(tempData2)/finalGap;  %spend per day
    else
        Validate(j)=0;    %no data? so 0
    end
    myMatVal(j,:)=myMatCostPer(j,:); %populate what existed
    XVal(j,1)=agep;
    XVal(j,2)=gendp;
    XVal(j,3)=max(myMatVal(j,:));
%    keyboard
    %drawnow
end